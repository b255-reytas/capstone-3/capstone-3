import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Login(props){

	// Allows us to consume the User context object and it's properties to use for user validation

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	function loginUser(e){
		e.preventDefault();

		// Process a fetch request to the corresponding backend API
		// The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
		// The fetch request will communicate with our backend application providing it with a stringified JSON
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			};
		})

		localStorage.setItem('email', email)

		setEmail('');
		setPassword('');

	}

		const retrieveUserDetails = (token) => {

			fetch('http://localhost:4000/users/details', {
				headers: {
					Authorization: `Bearer ${ token }`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				}, () => {
					console.log('User object', user)
				})
			})
		}

useEffect(() => {

		if((email !== '' && password !== '')){
			setIsActive(true);
		}  else {
			setIsActive(false);
		}

	}, [email, password])

	console.log(user.id)
	
	return (
		(user.id !== null) ?
		<Navigate to = "/"/>
		:
		<Form onSubmit = {(e) => loginUser(e)}>
			<h1>Login</h1>
	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	                value = {email}
	                onChange= {e => setEmail(e.target.value)}
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value = {password}
	                onChange = {e => setPassword(e.target.value)}
	                required
	            />
	        </Form.Group>

	        {isActive ? 
			    <Button variant="success" type="submit" id="submitBtn">
			        Login
			    </Button>
			    :
			    <Button variant="success" type="submit" id="submitBtn" disabled>
			        Login
			    </Button>
	        }
	    </Form>
		)
}