import { Fragment, useContext } from 'react';
import { NavLink } from 'react-router-dom';
import CreateProduct from '../pages/CreateProduct';
import AllProducts from '../pages/AllProducts';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Admin() {
  const { isAdmin } = useContext(UserContext);

  if (isAdmin === false) {
    return (
      <div>
        <p>Non-admin user, please contact User Administrator</p>
        <NavLink to="/">Return to Homepage</NavLink>
      </div>
      )
  } else {
  return (
    <Fragment>
      <CreateProduct />
      <AllProducts />
    </Fragment>
  )};
}