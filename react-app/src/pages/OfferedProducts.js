import React, { Fragment, useEffect, useState } from 'react';
import ActiveProducts from '../components/ActiveProducts';
import axios from 'axios';

export default function OfferedProducts(){

  const [ products, setProducts ] = useState([])
  const [ error, setError ] = useState(null)

useEffect(() => {
  axios
    .get(`${process.env.REACT_APP_API_URL}/products/`)
    .then(response => {
      console.log("response: ", response)
      setProducts(response.data);
    })
    .catch(error => console.log(error));
}, []);

  return (
    <Fragment>
        {products.map(product => (
        <ActiveProducts key={product._id} productProp={product} />
      ))}
    </Fragment>
  )
}