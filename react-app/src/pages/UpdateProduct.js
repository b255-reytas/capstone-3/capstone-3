import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useParams } from 'react-router-dom'
import axios from 'axios';
import Swal from 'sweetalert2';

export default function UpdateProduct() {

	  const [product, setProduct] = useState({});
	  const [name, setName] = useState('');
	  const [description, setDescription] = useState('');
	  const [price, setPrice] = useState('');
	  const [isActive, setIsActive] = useState(false);
	  const { id } = useParams();

  useEffect(() => {
    axios.get(`${ process.env.REACT_APP_API_URL }/products/${id}`)
      .then(response => {
        const { name, description, price, isActive } = response.data;
        setProduct(response.data);
        setName(name);
        setDescription(description);
        setPrice(price);
        setIsActive(isActive);
      })
      .catch(error => console.error(error));
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();
    const updatedProduct = {
      name,
      description,
      price,
      isActive,
    };
    axios.put(`${ process.env.REACT_APP_API_URL }/products/${product._id}`, updatedProduct, {
    	headers: {
    		Authorization: `Bearer ${localStorage.getItem("token")}`
    	}

    })
    
      .then(response => {
      if (response.data.success) {
          Swal.fire({
            title: 'Success',
            text: 'Product updated successfully!',
            icon: 'success',
          });
        } else {
          Swal.fire({
            title: 'Error',
            text: 'Product update failed!',
            icon: 'error',
      	});
      }
    })
      .catch(error => {
      console.error(error);
        Swal.fire({
          title: 'Error',
          text: 'Product update failed!',
          icon: 'error',
      });
    });
  };

  return (
    <div>
      <h1>Update Product</h1>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="formName">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formDescription">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            value={description}
            onChange={(event) => setDescription(event.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formPrice">
          <Form.Label>Price (&#8369;)</Form.Label>
          <Form.Control
            type="number"
            value={price}
            onChange={(event) => setPrice(event.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formIsActive">
          <Form.Check
            type="checkbox"
            label="Active"
            checked={isActive}
            onChange={(event) => setIsActive(event.target.checked)}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Update
        </Button>
      </Form>
    </div>
  );
}
