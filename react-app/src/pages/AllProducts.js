import React, { Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/Product';
import axios from 'axios';

export default function AllProducts(){

  const [ products, setProducts ] = useState([])
  const [ error, setError ] = useState(null)

useEffect(() => {
  axios
    .get(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(response => {
      console.log("response: ", response)
      setProducts(response.data);
    })
    .catch(error => console.log(error));
}, []);

  return (
    <Fragment>
        {products.map(product => (
        <ProductCard key={product._id} productProp={product} />
      ))}
    </Fragment>
  )
}