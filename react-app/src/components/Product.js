import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link, useNavigate } from 'react-router-dom';

export default function ProductCard({productProp}) {

    const {name, description, price, _id, isActive} = productProp
    const navigate = useNavigate();
    const handleEditProduct = () => {
            navigate(`/product/${_id}`);
        };

    return(

      <Card>
          <Card.Body>

              <Card.Title>{name}</Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>&#8369; {price}</Card.Text>

              <Card.Subtitle>Status:</Card.Subtitle>
              <Card.Text>{isActive ? "Active" : "Inactive"}</Card.Text>              
              <Card.Subtitle>ID:</Card.Subtitle>
              <Card.Text>{_id}</Card.Text>

              <Button variant="success" type="submit" id="submitBtn" onClick={() => navigate(`/product/${_id}`)}>Edit Product
              </Button>

          </Card.Body>
      </Card> 
      )
}



ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    isActive: PropTypes.bool.isRequired
  })
};