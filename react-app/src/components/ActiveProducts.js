import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link, useNavigate } from 'react-router-dom';

export default function ActiveProducts({productProp}) {

    const {name, description, price, isActive} = productProp;

    return(

      <Card>
          <Card.Body>

              <Card.Title>{name}</Card.Title>

              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>

              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>&#8369; {price}</Card.Text>
 
          </Card.Body>
      </Card> 
      )
}

ActiveProducts.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    isActive: PropTypes.bool.isRequired
  })
};