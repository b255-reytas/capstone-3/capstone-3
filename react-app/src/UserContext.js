import React from 'react';

// Creates a context object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other component within this app
// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;