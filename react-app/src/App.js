import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import React from 'react';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Admin from './pages/Admin';
import OfferedProducts from './pages/OfferedProducts'
import Checkout from './pages/Checkout';
import AllProducts from './pages/AllProducts';
import UpdateProduct from './pages/UpdateProduct'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import NotFound from './pages/NotFound';
import { useState, useEffect } from 'react';

// Admin
import Create from './pages/CreateProduct';

import './App.css';
import { UserProvider } from './UserContext';
import { ProductProvider } from './ProductContext'


function App() {
  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the local storage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  // Used to check if the user information is properly stored upon login
  useEffect(() =>{
    console.log(user);
    console.log(localStorage)
  }, [user])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
          <Container fluid>
              <AppNavBar />
              <Routes>
                  <Route path="/" element ={<Home />} />
                  <Route path="/register" element ={<Register />} />
                  <Route path="/products" element = {<AllProducts />}/>
                  <Route path="/offered" element = {<OfferedProducts />}/>

                  <Route path="/product/:id" element={<UpdateProduct />} />
                  <Route path="/admin" element ={<Admin />} />
                  <Route path="/checkout" element ={<Checkout />} />
                  <Route path="/login" element ={<Login />} />
                  <Route path="/logout" element ={<Logout />} />
                  <Route path= "*" element = {<NotFound />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}

export default App;

// Create route paths to the Course component, Register component, Login component
// Route names should be /course, /register, /login